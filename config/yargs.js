const argv = require('yargs')
                .option('b', {
                    alias: 'base',
                    type: 'number',
                    demandOption: true,
                    describe: 'Es la base de la tabla de multiplicar'
                })
                .option('l', {
                    alias: 'listar',
                    type: 'boolean',
                    demandOption: true,
                    default: false,
                    describe: 'Muestra la tabla en consola'
                })
                .option('h', {
                    alias: 'hasta',
                    type: 'number',
                    default: 10,
                    describe: 'Es el número hasta donde quieres la tabla'
                })
                .check( (argv, options) => {
                    if ( isNaN(argv.b) || isNaN(argv.h) ) {
                        throw 'La base o el límite tienen que ser un numericos'
                    }
                    return true;
                })
                .argv;

module.exports = argv;