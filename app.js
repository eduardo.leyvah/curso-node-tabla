const { crearArchivo } = require('./helpers/multiplicar')
const argv = require('./config/yargs')
require('colors')

// const base = 3;

console.clear();

// console.log(process.argv);

// console.log(argv);

// console.log('base: yargs', argv.b);

// const [ , , arg3 = 'base=5'] = process.argv;
// const [ , base = 5 ] = arg3.split('=');

// console.log(base);

crearArchivo(argv.b, argv.l, argv.h)
    .then( nombreArchivo => console.log(nombreArchivo.rainbow, 'creado'.rainbow))
    .catch( err => console.log(err) );
